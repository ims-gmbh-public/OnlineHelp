@REM %~dp0 is the task directory

mkdir %~dp0.\OnlineHelp

set GITCMD=git
if exist \portableGit\bin\git.exe set GITCMD=\portableGit\bin\git
@REM node4 is the tag to clone:
%GITCMD% clone --single-branch --branch master https://gitlab.com/ims-gmbh-public/OnlineHelp.git %~dp0.\onlineHelp\
@rem rd /S /Q %~dp0..\node_modules\.bin\.git

:end

