@REM %~dp0 is the task directory

if exist %~dp0..\node_modules\.bin\npm.cmd goto end

mkdir %~dp0..\node_modules
mkdir %~dp0..\node_modules\.bin

set GITCMD=git
if exist \portableGit\bin\git.exe set GITCMD=\portableGit\bin\git
@REM node4 is the tag to clone:
%GITCMD% clone --single-branch --branch node4 https://gitlab.com/ims-gmbh-public/NodeAndNpm.git %~dp0..\node_modules\.bin\
rd /S /Q %~dp0..\node_modules\.bin\.git

:end

